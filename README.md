# Bluetooth Tethering

There are some obsolete controls in this program, which will soon be removed. The important controls are at the bottom of the screen, "Auto Bluetooth Tethering", and "Auto Wifi Tethering".<br>
<br>
The function of the program is mainly to combat an irritating feature of Android where it disables bluetooth tethering every time you reboot the device, or re-enable bluetooth (after having been switched off). In addition, it is able to switch wifi tethering on automatically if it detects that it is connected to a specific BLUETOOTH device, signifying that you are in your car, and it needs to access data network via wifi.<br>
<br>
The bluetooth functionality simply requires you to switch on "Auto Bluetooth Tethering".<br>
<br>
For wifi, select the BLUETOOTH device(s) in the dropdown menu at the top, and then switch on "Auto Wifi Tethering".